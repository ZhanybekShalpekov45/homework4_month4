from django.urls import path
from . import views

urlpatterns = [
    path('tv_show/', views.tv_show_view, name='tv_show'),
    path('tv_show/<int:id>/', views.tv_show_detail_view, name='tv_show_detail'),
    path('tv_show/<int:id>/delete/', views.delete_tv_show_view, name='tv_show_delete'),
    path('tv_show/<int:id>/update/', views.update_tv_show_view, name='tv_show_update'),
    path('add-tv_show/', views.create_tv_show_view, name='add_tv_show'),
]
