# Generated by Django 4.2.4 on 2023-08-15 16:11

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TvShow',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='tv_show/')),
                ('description', models.TextField()),
                ('type_tv_show', models.CharField(choices=[('Комедийные', 'Комедийные'), ('Научные', 'Научные'), ('Новости', 'Новости'), ('Хроника', 'Хроника')], max_length=100)),
                ('url_tv_show', models.URLField()),
                ('company_tv', models.CharField(max_length=35)),
                ('age_restrictions', models.IntegerField()),
                ('number_tv_show', models.IntegerField()),
                ('instagram', models.URLField()),
                ('about_tv_show', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
