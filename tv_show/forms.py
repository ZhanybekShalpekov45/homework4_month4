from django import forms
from . import models

class Tv_Show_Form(forms.ModelForm):
    class Meta:
        model = models.TvShow
        fields = '__all__'