from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from . import models,forms


def tv_show_view(request):
    tv_show = models.TvShow.objects.all()
    return render(request, 'tv_show/tv_show.html', {'tv_show': tv_show})


def tv_show_detail_view(request, id):
    tv_show_id = get_object_or_404(models.TvShow, id=id)
    return render(request, 'tv_show/tv_show_detail.html',
                  {'tv_show_id': tv_show_id})



#Добавление объекта через формы CRUD
def create_tv_show_view(request):
    method = request.method
    if method == "POST":
        form = forms.Tv_Show_Form(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponse("Тв-шоу успешно добавлено в БД")
    else:
        form = forms.Tv_Show_Form()

    return render(request, 'tv_show/crud/add_tv_show.html',
                  {"form": form})

#Удаление из БД CRUD
def delete_tv_show_view(request, id):
    tv_show_id = get_object_or_404(models.TvShow, id=id)
    tv_show_id.delete()
    return HttpResponse('Тв-шоу успешно удалено из БД')

#Изменение объектов в CRUD


def update_tv_show_view(request,id):
    tv_show_id = get_object_or_404(models.TvShow, id=id)
    if request.method == "POST":
        form = forms.Tv_Show_Form(instance=tv_show_id, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse('Успешно обновлено')
    else:
        form = forms.Tv_Show_Form(instance=tv_show_id)

    context = {
        'form': form,
        'tv_show_id': tv_show_id
    }
    return render(request, 'tv_show/crud/update_tv_show.html', context)
